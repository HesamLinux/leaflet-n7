module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  /**
   * Needed to avoid a misconfiguration of babel + storybook
   * https://github.com/storybookjs/storybook/issues/14805#issuecomment-982465674
   */
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    ['@babel/plugin-proposal-private-methods', { loose: true }],
    ['@babel/plugin-proposal-private-property-in-object', { loose: true }]
  ]
}
