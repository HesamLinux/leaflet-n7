import { mount } from '@vue/test-utils'
import AddressForm from './AddressForm.vue'

const fields = [{
  name: 'road',
  label: 'Road',
  title: 'Road',
  type: 'text',
  style: {
    color: 'red'
  }
}, {
  name: 'zipCode',
  label: 'Zip Code',
  title: 'Zip Code',
  type: 'number',
  style: {
    backgroundColor: 'green'
  }
}, {
  name: 'city',
  label: 'City',
  title: 'City',
  type: 'text'
}, {
  name: 'complement',
  label: 'Complement',
  title: 'Complement',
  type: 'text'
}, {
  name: 'country',
  label: 'Country',
  title: 'Country',
  type: 'text'
}, {
  name: 'state',
  label: 'State',
  title: 'State',
  type: 'text'
}]
const addressToEdit = {
  title: 'My address',
  city: 'Nantes',
  zipCode: '44000',
  road: 'rue du Marchix',
  state: 'France'
}

describe('AddressForm.vue', () => {
  it('renders without crashing', () => {
    const wrapper = mount(AddressForm)
    expect(wrapper).toBeDefined()
  })
  it('renders with props', () => {
    const wrapper = mount(AddressForm, {
      propsData: {
        addressToEdit,
        fields
      }
    })
    expect(wrapper).toBeDefined()
    expect(wrapper.text()).toContain('My address')
    expect(wrapper.html()).toMatchSnapshot()
  })
  it('emit the \'update:addressToEdit\' event', async () => {
    const wrapper = mount(AddressForm, {
      propsData: {
        addressToEdit,
        fields
      },
    })
    expect(wrapper).toBeDefined()
    /**
     * find a cell and click on it
     * we add 2 for number of rows / cells
     * because we have a row and a column for Player tips
     * and because we have 0 based arrays
    */
    const wrapperInput = wrapper.find('input')
    expect(wrapperInput).toBeDefined()
    wrapperInput.setValue('pouet')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()).toHaveProperty('update:addressToEdit')
    expect(wrapper.emitted()['update:addressToEdit']).toHaveLength(1)
    expect(wrapper.emitted()['update:addressToEdit'][0][0])
      .toMatchObject({
        ...addressToEdit,
        [fields[0].name]: 'pouet'
      })
  })

})
