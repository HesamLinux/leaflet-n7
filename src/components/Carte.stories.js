import Carte from './Carte.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'components/Carte',
  component: Carte,
  // parameters: { actions: { argTypesRegex: '^on.*' } },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { Carte },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup() {
    return {
      args,
    };
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: `
    <Carte v-bind="args">
      <template v-slot:popup>
        This is the slot driven by storybook
      </template>
    </Carte>
  `
});

/**
 * Default story, without prop, need to work
 */
export const Default = Template.bind({});
Default.parameters = {
  storyshots: { disable: true },
};
/**
 * With coordinates
 */
const coordinates = [{"type":"Feature","geometry":{"type":"Point","coordinates":[43.60449,1.454132]},"properties":{"label":"Rue Pierre-Paul Riquet 31000 Toulouse","score":0.6209899999999999,"id":"31555_7496","name":"Rue Pierre-Paul Riquet","postcode":"31000","citycode":"31555","x":575159.34,"y":6279603.88,"city":"Toulouse","context":"31, Haute-Garonne, Occitanie","type":"street","importance":0.83089}},{"type":"Feature","geometry":{"type":"Point","coordinates":[43.607363,1.453929]},"properties":{"label":"Place Pierre-Paul Riquet 31000 Toulouse","score":0.51174,"id":"31555_7492","name":"Place Pierre-Paul Riquet","postcode":"31000","citycode":"31555","x":575149.2,"y":6279923.45,"city":"Toulouse","context":"31, Haute-Garonne, Occitanie","type":"street","importance":0.67914}},{"type":"Feature","geometry":{"type":"Point","coordinates":[43.606468,1.455725]},"properties":{"label":"Boulevard Pierre-Paul Riquet 31000 Toulouse","score":0.48005000000000003,"id":"31555_7488","name":"Boulevard Pierre-Paul Riquet","postcode":"31000","citycode":"31555","x":575292.27,"y":6279821.16,"city":"Toulouse","context":"31, Haute-Garonne, Occitanie","type":"street","importance":0.78055}}]
export const WithCoordinates = Template.bind({})
WithCoordinates.args = {
  coordinates,
  currentCoordinate: coordinates[0]
}
WithCoordinates.parameters = {
  storyshots: { disable: true },
};
