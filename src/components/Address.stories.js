import Address from './Address.vue'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'components/Address',
  component: Address,
  argTypes: {
    onEditAddress: {},
    onSearchAddress: {}
  }
}

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args) => ({
  // Components used in your story `template` are defined in the `components` object
  components: { Address },
  // The story's `args` need to be mapped into the template through the `setup()` method
  setup () {
    return { args }
  },
  // And then the `args` are bound to your component with `v-bind="args"`
  template: '<Address v-bind="args" />'
})

/**
 * Default story, without prop, need to work
 */
export const Default = Template.bind({})

/**
 * With an address in prop
 */
export const WithProp = Template.bind({})
WithProp.args = {
  address: {
    title: 'My address',
    city: 'Nantes',
    zipCode: '44000',
    road: 'rue du Marchix',
    state: 'France'
  },
  label: 'This is a label'
}

/**
 * With component in english
 */
export const InEnglish = Template.bind({})
InEnglish.args = {
  address: {
    title: 'My address',
    city: 'Nantes',
    zipCode: '44000',
    road: 'rue du Marchix',
    state: 'France'
  },
  label: 'This is a label'
}
InEnglish.decorators = [
  () => ({
    template: '<div style="background-color: red"><story /></div>',
    mounted () {
      this.$i18n.locale = 'en'
    }
  })
]

/**
 * With component in french
 */
export const InFrench = Template.bind({
})
InFrench.args = {
  address: {
    title: 'My address',
    city: 'Nantes',
    zipCode: '44000',
    road: 'rue du Marchix',
    state: 'France'
  },
  label: 'This is a label'
}
InFrench.decorators = [
  () => ({
    template: '<story />',
    mounted () {
      this.$i18n.locale = 'fr'
    }
  })
]
